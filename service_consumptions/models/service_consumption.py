# -*- coding: utf-8 -*-

from datetime import datetime

from odoo import api, fields, models, _


class Serviceconsumption(models.Model):
    _name = 'service.consumption'
    _description = 'Service consumptions'

    name = fields.Char(string="name", readonly=True)
    timestamp = fields.Integer(string='Timestamp')
    timestamp_datetime = fields.Datetime(string='Timestamp')
    product_id = fields.Many2one(string='Product', comodel_name='product.product')
    quantity = fields.Integer(string='Quantity')
    category_code = fields.Char(related='product_id.categ_id.code', string='Category', store=True)

    @api.model
    def create(self, vals):
        vals['name'] = self.name = self.env['ir.sequence'].next_by_code('service.consumption') or _('New')
        if vals.get('timestamp'):
            vals['timestamp_datetime'] = datetime.fromtimestamp(vals.get('timestamp'))
        return super().create(vals)
