# -*- coding: utf-8 -*-
{
    'name': 'Service consumptions',

    'summary': """
        Odoo technical interview""",

    'description': """
        Managing telecom service consumptions
        =====================================
        Managing telecom service consumptions
    """,

    'author': 'Aleix Capdevila',
    "website": "",

    'category': 'Others',
    'version': '14.0.1.0.0',

    'depends': [
        'product',
    ],

    'data': [
        'security/ir.model.access.csv',
        'data/ir_sequence.xml',
        'views/service_consumption_views.xml',
        'views/product_category_view.xml',
        'views/product_product_views.xml',
    ],
    'demo': [
        'demo/product.category.csv',
        'demo/product.product.csv',
        'demo/service.consumption.csv',
    ],
    "installable": True,
    "application": True,
}
